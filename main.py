import pygame
import math
import random
pygame.mixer.pre_init(44100,16,2,4096)
pygame.init()

screen = pygame.display.set_mode((800,600))

font_style =pygame.font.SysFont("italic", 30)

pygame.mixer.music.load("mus.mp3")
pygame.mixer.music.set_volume(0.5)
pygame.mixer.music.play(-1)

pygame.display.set_caption("game1")
icon = pygame.image.load('boat.png')
black = (0, 0, 0)
blue = (0, 0, 255)
RED = (194 ,178, 128)
pygame.display.set_icon(icon)
obstac1Img = pygame.image.load('barrierrest.png')
obstac2Img = pygame.image.load('barrierrest.png')
obstac3Img = pygame.image.load('barrierrest.png')
obstac4Img = pygame.image.load('barrierrest.png')
#obstac1X=randint
obstac1X=100
obstac1Y=268
obstac2X=450
obstac2Y=468
obstac3X=600
obstac3Y=168
obstac4X=250
obstac4Y=68
player1Img = pygame.image.load('ship.png')
player1X=368
player1Y=568
player2Img = pygame.image.load('ship.png')
player2X=368
player2Y=0
raft1Img = pygame.image.load('raft.png')
raft1X=0
raft1Y=100
raft1X_change = 0.6
raft1Y_change = 0
player1X_change = 0
player1Y_change = 0
player2X_change = 0
player2Y_change = 0
raft2Img = pygame.image.load('raft.png')
raft2X=0
raft2Y=300
raft2X_change = 0.4
raft2Y_change = 0
raft3Img = pygame.image.load('raft.png')
raft3X=768
raft3Y=400
raft3X_change = 0.8
raft3Y_change = 0
#Score

score_val = 0
font = pygame.font.Font('freesansbold.ttf',32)
textX = 10
textY = 10
def show_score(x,y):
    score = font.render("Score :" + str(score_val),True,(0,0,0))
    screen.blit(score,(x,y))
def obstac1(x,y):
    screen.blit(obstac1Img,(x,y))
def obstac2(x,y):
    screen.blit(obstac2Img,(x,y))
def obstac3(x, y):
    screen.blit(obstac3Img, (x, y))
def obstac4(x,y):
    screen.blit(obstac4Img,(x,y))
def player1(x,y):
    screen.blit(player1Img,(x,y))
def player2(x,y):
    screen.blit(player2Img,(x,y))
def raft1(x,y):
    screen.blit(raft1Img,(x,y))
def raft2(x,y):
    screen.blit(raft2Img,(x,y))
def raft3(x,y):
    screen.blit(raft3Img,(x,y))
def isCollision(enemyX,enemyY,pX,pY):
    distance = math.sqrt((math.pow(enemyX-pX,2))+(math.pow(enemyY-pY,2)))
    if distance < 31:
        return True
    else:
        return False
c=0
running = True
pause = False
while 1:
    while running:
        if player1Y > 468:
            score_val = 0
        elif player1Y <= 468 and player1Y > 400:
            score_val = 5
        elif player1Y <= 400 and player1Y > 300:
            score_val = 15
        elif player1Y <= 300 and player1Y > 268:
            score_val = 25
        elif player1Y <= 268 and player1Y > 168:
            score_val = 30
        elif player1Y <= 168 and player1Y > 100:
            score_val = 35
        elif player1Y <= 100 and player1Y > 68:
            score_val = 45
        elif player1Y <= 68  :
            score_val = 50

        #info = font_style.render("SCORE=0",True,((0,0,0)))
        #screen.blit(info,[10,0])
        #pygame.display.update()
        while pause:
            screen.fill((0, 0, 0))
            info = font_style.render("Game Paused , press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pause = False
                    running = False
                    break
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        pause = False
        screen.fill((0, 119, 190))
        pygame.draw.rect(screen, RED, (0, 268, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 68, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 168, 800, 32), 0)
    #pygame.draw.rect(screen, RED, (0, 368, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 468, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 0, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 568, 800, 32), 0)
    #pygame.display.update()

        #while raftX < 768:
    #raftX += 0.1
        #while raftX >= 0:
         #   raftX -=  0.1


    #if:
    #raftX -= 0.5
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                running = False

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    player1X_change = -0.2
                elif event.key == pygame.K_RIGHT:
                    player1X_change = 0.2
                elif event.key == pygame.K_UP:
                    player1Y_change = -0.2
                elif event.key == pygame.K_DOWN:
                    player1Y_change = +0.2
                elif event.key == pygame.K_ESCAPE:
                    pause = True

            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT or event.key == pygame.K_DOWN or event.key == pygame.K_UP:
                    player1X_change = 0
                    player1Y_change = 0


    ################################
        player1X += player1X_change

        if player1X<=0:
            player1X=0
        elif player1X>=768:
            player1X=768

        player1Y += player1Y_change

        if player1Y <= 0:
            player1Y = 0
        elif player1Y >= 568:
            player1Y = 568

        raft1X += raft1X_change

        if raft1X <= 0:
            raft1X_change = 1.8
            #raft1Y_change = 0.4
        elif raft1X >= 768:
            raft1X_change = -1.8

        raft2X += raft2X_change

        if raft2X <= 0:
            raft2X_change = 1.9
        elif raft2X >= 768:
            raft2X_change = -1.1

        raft3X += raft3X_change

        if raft3X <= 0:
            raft3X_change = 2.4
        elif raft3X >= 768:
            raft3X_change = -1.2


        collision1 =isCollision(obstac1X,obstac1Y,player1X,player1Y)
        while collision1:
            screen.fill((0,0,0))
            PS1 = 25
            info = font_style.render("player1 score is 25, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""

                        collision1 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        collision2 = isCollision(obstac2X, obstac2Y, player1X, player1Y)
        while collision2:
            screen.fill((0, 0, 0))
            PS1 = 0
            info = font_style.render("player1 score is 0 ,press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collision2 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break


        collision3 = isCollision(obstac3X, obstac3Y, player1X, player1Y)
        while collision3:
            screen.fill((0, 0, 0))
            PS1 = 30
            info = font_style.render("player1 score is 30, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collision3 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        collision4 = isCollision(obstac4X, obstac4Y, player1X, player1Y)
        while collision4:
            screen.fill((0, 0, 0))
            PS1 = 45
            info = font_style.render("player1 score is 45, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collision4 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        collisionr3 = isCollision(raft3X,raft3Y,player1X,player1Y)
        while collisionr3:
            screen.fill((0, 0, 0))
            PS1 = 5
            info = font_style.render("player1 score is 5, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collisionr3 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        collisionr2 = isCollision(raft2X, raft2Y, player1X, player1Y)
        while collisionr2:
            screen.fill((0, 0, 0))
            PS1 = 15
            info = font_style.render("player1 score is 15, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collisionr2 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        collisionr1 = isCollision(raft1X, raft1Y, player1X, player1Y)
        while collisionr1:
            screen.fill((0, 0, 0))
            PS1 = 35
            info = font_style.render("player1 score is 35, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        collisionr1 = False
                        running = False
                        running2 = True
                        player2Y = 0
                        break

        while player1Y <= 1 and player1Y >0:
            #print("hiiiiiii")

            screen.fill((0,0,0))
            PS1 = 50
            info = font_style.render("player1 completed the level,your score is 50,press space to continue",True,((255,25,100)))
            screen.blit(info,[100,500])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        player1Y = 0
                        player2Y = 0
                        running = False
                        running2 = True
                        break

        player1(player1X, player1Y)
        obstac1(obstac1X, obstac1Y)
        obstac2(obstac2X, obstac2Y)
        obstac3(obstac3X, obstac3Y)
        obstac4(obstac4X, obstac4Y)
        raft1(raft1X,raft1Y)
        raft2(raft2X, raft2Y)
        raft3(raft3X, raft3Y)
        show_score(textX,textY)
        pygame.display.update()









    player1Y_change = 0
    player1X_change = 0
    running2 = True
    while running2:
        if player2Y > 468:
            score_val = 50
        elif player2Y <= 468 and player2Y > 400:
            score_val = 45
        elif player2Y <= 400 and player2Y > 300:
            score_val = 35
        elif player2Y <= 300 and player2Y > 268:
            score_val = 25
        elif player2Y <= 268 and player2Y > 168:
            score_val = 20
        elif player2Y <= 168 and player2Y > 100:
            score_val = 15
        elif player2Y <= 100 and player2Y > 68:
            score_val = 5
        elif player2Y <= 68  :
            score_val = 0

        #info = font_style.render("SCORE=0",True,((0,0,0)))
        #screen.blit(info,[10,0])
        #pygame.display.update()
        while pause:
            screen.fill((0, 0, 0))
            info = font_style.render("Game Paused , press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pause = False
                    running2 = False
                    break
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        pause = False
        screen.fill((0, 119, 190))
        pygame.draw.rect(screen, RED, (0, 268, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 68, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 168, 800, 32), 0)
        #pygame.draw.rect(screen, RED, (0, 368, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 468, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 0, 800, 32), 0)
        pygame.draw.rect(screen, RED, (0, 568, 800, 32), 0)
        #pygame.display.update()

            #while raftX < 768:
        #raftX += 0.1
            #while raftX >= 0:
             #   raftX -=  0.1


        #if:
        #raftX -= 0.5
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                running2 = False


            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    player1X_change = -0.2
                elif event.key == pygame.K_RIGHT:
                    player1X_change = 0.2
                elif event.key == pygame.K_UP:
                    player1Y_change = -0.2
                elif event.key == pygame.K_DOWN:
                    player1Y_change = +0.2
                elif event.key == pygame.K_ESCAPE:
                    pause = True

            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT or event.key == pygame.K_DOWN or event.key == pygame.K_UP:
                    player1X_change = 0
                    player1Y_change = 0


        ################################
        player2X += player1X_change
        if player2X<=0:
            player2X=0
        elif player2X>=768:
            player2X=768
        player2Y += player1Y_change
        if player2Y <= 0:
            player2Y = 0
        elif player2Y >= 568:
            player2Y = 568

        raft1X += raft1X_change

        if raft1X <= 0:
            raft1X_change = 2
        elif raft1X >= 768:
            raft1X_change = -1.7

        raft2X += raft2X_change

        if raft2X <= 0:
            raft2X_change = 1.1
        elif raft2X >= 768:
            raft2X_change = -1.6

        raft3X += raft3X_change

        if raft3X <= 0:
            raft3X_change = 1.5
        elif raft3X >= 768:
            raft3X_change = -1.4

        collision1 =isCollision(obstac1X,obstac1Y,player2X,player2Y)
        while collision1:
            screen.fill((0,0,0))
            PS2 = 20
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 20, press Space to continue", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0
                        collision1 = False
                        break"""
                        """pygame.quit()
                        quit()"""
                        collision1 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        collision2 = isCollision(obstac2X, obstac2Y, player2X, player2Y)
        while collision2:
            screen.fill((0, 0, 0))
            PS2 = 45
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 45,press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0
                        collision2 = False
                        break"""
                        """pygame.quit()
                        quit()"""
                        collision2 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break



        collision3 = isCollision(obstac3X, obstac3Y, player2X, player2Y)
        while collision3:
            screen.fill((0, 0, 0))
            PS2 = 15
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 15, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0
                        collision3 = False
                        break"""
                        """pygame.quit()
                        quit()"""
                        collision3 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        collision4 = isCollision(obstac4X, obstac4Y, player2X, player2Y)
        while collision4:
            screen.fill((0, 0, 0))
            PS2 = 0
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 0, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0
                        collision4 = False
                        break"""
                        """pygame.quit()
                        quit()"""
                        collision4 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        collisionr3 = isCollision(raft3X, raft3Y, player2X, player2Y)
        while collisionr3:
            screen.fill((0, 0, 0))
            PS2 = 35
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 35, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        """collisionr3 = False
                        running = False
                        running2 = True"""
                        """pygame.quit()
                        quit()"""
                        collisionr3 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        collisionr2 = isCollision(raft2X, raft2Y, player2X, player2Y)
        while collisionr2:
            screen.fill((0, 0, 0))
            PS2 = 25
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 25, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        """collisionr2 = False
                        running = False
                        running2 = True"""
                        """pygame.quit()
                        quit()"""
                        collisionr2 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        collisionr1 = isCollision(raft1X, raft1Y, player2X, player2Y)
        while collisionr1:
            screen.fill((0, 0, 0))
            PS2 = 5
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 score is 5, press Space to CONTINUE", True, ((255, 255, 255)))
            screen.blit(info, [212, 300])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0"""
                        """collisionr1 = False
                        running = False
                        running2 = True"""
                        """pygame.quit()
                        quit()"""
                        collisionr1 = False
                        running2 = False
                        running = True
                        player1Y = 568
                        break

        while player2Y >= 568:
        #while player2Y 569 and player2Y >569:
            #print("hiiiiiii")

            screen.fill((0,0,0))
            PS2 = 50
            if PS1 > PS2:
                info = font_style.render("player1 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            elif PS1 == PS2:
                info = font_style.render("TIED", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            else:
                info = font_style.render("player2 WON", True, ((255, 255, 255)))
                screen.blit(info, [212, 100])
                #pygame.display.update()
            info = font_style.render("player2 completed the level,your score is 50,press space to CONTINUE",True,((255,0,0)))
            screen.blit(info,[100,500])
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                     pygame.quit()
                     quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        """player1X = 368
                        player1Y = 0
                        player1Y_change = 0
                        player1X_change = 0
                        break"""
                        """pygame.quit()
                        quit()"""
                        player1Y = 568
                        player2Y = 0
                        running2 = False
                        running = True
                        break

        player2(player2X, player2Y)
        obstac1(obstac1X, obstac1Y)
        obstac2(obstac2X, obstac2Y)
        obstac3(obstac3X, obstac3Y)
        obstac4(obstac4X, obstac4Y)
        raft1(raft1X,raft1Y)
        raft2(raft2X, raft2Y)
        raft3(raft3X, raft3Y)
        show_score(textX,textY)
        pygame.display.update()

    player1Y_change = 0
    player1X_change = 0
